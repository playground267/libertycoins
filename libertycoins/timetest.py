from .assesor.collect import coin_data
from .utils.decorators import time_performance


@time_performance
def collect_coin_data():
    # for value in coin_data:
    #     print(value)
    coin_data
    

if __name__ == '__main__':
    collect_coin_data()