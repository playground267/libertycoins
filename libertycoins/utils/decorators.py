import time, functools


def time_performance(func):
    """
    Print runtime of decorated function
    """ 
    # preserve function information 
    @functools.wraps(func)
    def tp_wrapper(*args, **kwargs):
        start_time = time.perf_counter()
        value = func(*args, **kwargs)
        end_time = time.perf_counter()
        run_time = end_time - start_time
        print(f"Finished {func.__name__!r} in {run_time:.4f} secs")
        return value   
    return tp_wrapper

def debug(func):
    """
    Print the function signature and return value 
    """
    @functools.wraps(func)
    def debug_wrapper(*args, **kwargs):
        args_repr = [repr(a) for a in args]
        kwargs_repr = [f"{k}= {v!r}" for k, v in kwargs.items()]
        signature = ", ".join(args_repr + kwargs_repr)
        print(f"Calling {func.__name__}({signature})")
        value = func(*args, **kwargs)
        print(f"{func.__name__!r} returned {value!r}")
        return value
    return debug_wrapper



# def decorator(func):
#     @functools.wraps(func)
#     def wrapper_decorator(*args, **kwargs):
#         # do something before 
#         value = func(*args, **kwargs)
#         # do something after
#         return value
#     return wrapper_decorator

