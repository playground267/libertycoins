import argparse
import pandas as pd

from assesor.collect import open_local



parser = argparse.ArgumentParser(prog="libertycoins")
parser.add_argument("year", type=str)
args = parser.parse_args()

year = args.year
value_matrix = open_local()
date = value_matrix[0]
values = value_matrix[1]

results_by_year = [coin for coin in values if coin['Date'].split(' ')[0] == year]
print(f"Results for {date}")

result_table = pd.DataFrame(results_by_year)
print(result_table) 


