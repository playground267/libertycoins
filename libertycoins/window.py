import sys

from PyQt6.QtWidgets import (
    QApplication,
    QGridLayout,
    QComboBox,
    QPushButton,
    QLabel,
    QLineEdit,
    QWidget,
)

# from functions import get_value
def get_value():
    if displayValue_lbl.text():
        displayValue_lbl.setText("")
    else:
        displayValue_lbl.setText("These are values")

app = QApplication([])

window = QWidget()
window.setWindowTitle("Coin Values")
window.setGeometry(800, 100, 200, 100)

layout = QGridLayout()

# Create comboboxes for the mint year and mint mark of each coin 
mintYear_cb = QComboBox()
mintMark_cb = QComboBox()
mintYear_cb.addItems([str(year) for year in range(1877, (1907 + 1))])
mintMark_cb.addItems(["-- Mint Mark --", "No Mark", "CC", "S"])

getValue_btn = QPushButton("Get Value")
getValue_btn.clicked.connect(get_value)
displayValue_lbl = QLabel("")

layout.addWidget(mintYear_cb, 0, 0)
layout.addWidget(mintMark_cb, 0, 1)
layout.addWidget(getValue_btn, 1, 0)
layout.addWidget(displayValue_lbl, 1, 1)

window.setLayout(layout)
window.show()

sys.exit(app.exec())


