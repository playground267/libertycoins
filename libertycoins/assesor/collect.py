import json
from datetime import datetime
from requests_html import HTMLSession
from typing import List, Dict
import os


history_path = os.path.dirname('./libertycoins/history/')

url = 'https://www.coinstudy.com/liberty-twenty-dollar-gold-coin-values.html'

def grab_table():
    session = HTMLSession()
    r = session.get(url)
    # Get first (in this case only) table
    table = r.html.find('table', first=True)
    return table
    
        
def open_local(): 
    hist_files = [datefile for datefile in os.listdir(history_path)]
    most_recent = sorted(hist_files, key=lambda x: x.split('.')).pop()
    with open(f"{history_path}/{most_recent}", 'r') as f:
        value_matrix = json.load(f)
    return value_matrix
    

def parse_table_for_processing(table) -> List[Dict[str, str]]:
    # Get table rows
    tabledata = [tuple(c.text for c in row.find('td')) for row in table.find('tr')][4:]

    # Get and format header row
    tableheader = [[c.text for c in row.find('th')] for row in table.find('tr')]
    tableheaders = [item.split('\n') for item in tableheader[2]]
    tableheaders = tuple(' '.join(item) for item in tableheaders)

    # Map header data row to coin data rows
    return [dict(zip(tableheaders, t)) for t in tabledata]

def create_file(data):
    with open(f"{history_path}/{datetime.now()}.json", 'w') as f:
        json.dump(data, f, indent=4, default=str)


def value_matrix() -> List[Dict[str, str]]: 
    print("Connecting to Live values table...\n")
    try:
        return (datetime.now(), parse_table_for_processing(grab_table()))
    except:
        print("Having trouble connecting. Using local history file\n")
        return open_local()



if __name__ == '__main__':
    # main()
    # print(open_local())
    create_file(value_matrix())
