**Install pipenv**
`pip install pipenv`


**Activate the Virtual Environment**
`pipenv shell`

**Install all packages**
`pipenv install`

**get a certain years coin**
(from CoinValues)
`python3.10 libertycoins <year>`

**create a history file**
`python3.10 -m libertycoins.assesor.collect`
